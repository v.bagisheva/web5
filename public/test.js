/*global
    alert
*/
function CLICK() {
    let price = document.getElementById("price").value;
    let quantity = document.getElementById("quantity").value;
    let res = document.getElementById("res");

    if ((/^[1-9][0-9]*$/.test(price)) === false) {
        alert("Проверьте ввод");
        return;
    }
    price = parseInt(price);

    if ((/^[1-9][0-9]*$/.test(quantity)) === false) {

        alert("Проверьте ввод");
        return;
    }
    quantity = parseInt(quantity);
    res.innerHTML = "Стоимость: " + price * quantity + " руб.";
}

window.addEventListener("DOMContentLoaded", function () {
    let bot = document.getElementById("count");
    bot.addEventListener("click", CLICK);
});
